> This repository has been superseded by [L4acados](https://github.com/IntelligentControlSystems/l4acados), which contains an improved zero-order GP-MPC implementation.

------

# Zero-Order GP-MPC

This repository contains the code for the paper "Zero-Order Optimization for Gaussian Process-based Model Predictive Control", accepted to the 2023 20th European Control Conference and published in the ECC 2023 Special Issue of the European Journal of Control (EJC). The article is available at https://www.sciencedirect.com/science/article/pii/S0947358023000912.

## Installation instructions

1. Clone this repository to a folder of your choice.
    ```bash
        git clone https://gitlab.ethz.ch/ics/zero-order-gp-mpc <yourpath>/zero-order-gp-mpc-code
    ```
    In the following we will refer to the root folder of the repository as `<yourpath>/zero-order-gp-mpc-code`.
2. Initialize submodules
    ```bash
        git submodule update --recursive --init
    ```
3. Compile `acados` according to the [`acados` documentation](https://docs.acados.org/installation/index.html)
    1. Change into `acados` directory
        ```bash
            cd acados
        ```
    2. Compile according to the [`acados` documentation](https://docs.acados.org/installation/index.html)
        ```bash
            mkdir -p build
            cd build
            cmake -DACADOS_PYTHON=ON .. # do not forget the ".."
            make install -j4 
        ```
4. Set up Python environment with `pipenv` (or any other tool you like), based on Python version `3.9.14`.
    1. Change into main directory, then
        ```bash
            pipenv install
        ```
    2. Export the variables `ACADOS_SOURCE_DIR` and `LD_LIBRARY_PATH` point towards the right locations in your `acados` installation:
        ```bash
            export ACADOS_SOURCE_DIR=<yourpath>/zero-order-gp-mpc-code/acados
            export LD_LIBRARY_PATH=<yourpath>/zero-order-gp-mpc-code/acados/lib
        ```
        In `pipenv`, this can be done by defining a file called `.env` in the root directory, e.g., `<yourpath>/zero-order-gp-mpc-code/.env`, which contains
        ```bash
            ACADOS_SOURCE_DIR=<yourpath>/zero-order-gp-mpc-code/acados
            LD_LIBRARY_PATH=<yourpath>/zero-order-gp-mpc-code/acados/lib
        ```
5. Run the script from within the environment (where also `ACADOS_SOURCE_DIR` and `LD_LIBRARY_PATH` are defined). In `pipenv`, it would go as follows:
    1. Enter environment:
        ```bash
            pipenv shell
        ```
    2. Verify that `ACADOS_SOURCE_DIR` and `LD_LIBRARY_PATH` are defined:
        ```bash
            echo $ACADOS_SOURCE_DIR # <yourpath>/zero-order-gp-mpc-code/acados
            echo $LD_LIBRARY_PATH # <yourpath>/zero-order-gp-mpc-code/acados/lib
        ```
    3. Run experiment:
        ```bash
            cd exp/hanging_chain/
            python main.py
        ```
        > At the first execution, you might be asked by `acados` to install `tera_renderer`.
    4. Generate figures by running
        ```bash
            python eval_timings_vary_nmass.py # Fig. 2
            python eval_timings_per_task.py # Fig. 3
        ```

## Dedicated repository for source code

If you are just interested in the source code, not the experiments, consider the further developed version of the software on GitHub: https://github.com/lahramon/zero-order-gpmpc.

## Citing us

```
@article{lahr_zero-order_2023,
  title = {Zero-Order Optimization for {{Gaussian}} Process-Based Model Predictive Control},
  author = {Lahr, Amon and Zanelli, Andrea and Carron, Andrea and Zeilinger, Melanie N.},
  year = {2023},
  journal = {European Journal of Control},
  pages = {100862},
  issn = {0947-3580},
  doi = {10.1016/j.ejcon.2023.100862}
}
```

