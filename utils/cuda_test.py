import torch
import numpy as np

if torch.cuda.is_available():
  dev = "cuda:0"
else:
  dev = "cpu"  

device = torch.device(dev)
print(dev)


torch.backends.cudnn.benchmark=True
#print(torch.cuda.is_available())

#dtype = 
#print(torch.cuda.device_count())

#number of samples to test
N = 5000
time = []
#torch.cuda.set_device(0)
start = torch.cuda.Event(enable_timing=True)
end = torch.cuda.Event(enable_timing=True)
for ii in range(3,N):
    
    start = torch.cuda.Event(enable_timing=True)
    end = torch.cuda.Event(enable_timing=True)

    tensor1 = torch.randn(ii, 1, 500).to(device)
    tensor2 = torch.randn(ii, 500, 1).to(device)
    #print(tensor1.shape, tensor2.shape) 
    start.record()
    torch.matmul(tensor1, tensor2)
    end.record()

    torch.cuda.synchronize()
    time.append(start.elapsed_time(end))


import matplotlib.pyplot as plt
plt.semilogy(np.arange(3,N)[1:], time[1:])
plt.show()
