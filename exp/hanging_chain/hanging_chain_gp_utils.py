# ©2022-​2023 ETH Zurich, Amon Lahr, Andrea Zanelli, Andrea Carron, Melanie N. Zeilinger; D-MAVT; Institute for Dynamical Systems and Control; Intelligent Control Systems Group

import sys, os
sys.path += ["../../src/"]

import torch
from torch import Tensor
from torch import load as torch_load
from torch.cuda import is_available as torch_cuda_is_available
from gpytorch.likelihoods import MultitaskGaussianLikelihood
from acados_template import AcadosOcpSolver

from utils import *
from hanging_chain_model_acados import export_ocp
from export_chain_mass_integrator import export_chain_mass_integrator
from gp_hyperparam_training import generate_train_data_acados, generate_train_inputs_zoro, generate_train_outputs_at_inputs, train_gp_model, get_gp_param_names_values, set_gp_param_value
from gp_model import MultitaskGPModel, BatchIndependentMultitaskGPModel
from zoro_acados import ZoroAcados

def generate_train_data_from_chain_params(chain_params, chain_params_aux=None):
    # auxiliary stuff
    if chain_params_aux is None:
        chain_params_aux = get_chain_params_utilities(chain_params)

    # get integrators nominal
    model = export_chain_mass_model(
        chain_params["n_mass"],
        chain_params["m"],
        chain_params["D"],
        chain_params["L"],
        wind_amp = 0.0
    )
    acados_integrator,_ = export_chain_mass_integrator(
        model,
        chain_params["Ts"]
    )

    # actual
    model_actual = export_chain_mass_model(
        chain_params["n_mass"],
        chain_params["m"],
        chain_params["D"],
        chain_params["L"],
        wind_amp = chain_params["wind_amp"]
    )
    acados_integrator_actual,_ = export_chain_mass_integrator(
        model_actual,
        chain_params["Ts"]
    )

    # init zoro solver
    ocp, sim = export_ocp(
        chain_params,
        chain_params_aux=chain_params_aux,
        include_wind=False
    )

    if chain_params["gp_train_params"]["solver"] == "nominal":
        acados_ocp_solver = AcadosOcpSolver(ocp, json_file = 'acados_ocp_' + ocp.model.name + '.json')
        x_train, y_train = generate_train_data_acados(acados_ocp_solver, 
            acados_integrator,
            acados_integrator_actual,
            chain_params_aux["x0"], 
            chain_params_aux["Sigma_W"], 
            chain_params["gp_train_params"]["N_sim_per_x0"], 
            chain_params["gp_train_params"]["N_sim"],
            B=chain_params_aux["B"],
            N_x0=chain_params["gp_train_params"]["N_x0"], 
            random_seed=chain_params["gp_train_params"]["random_seed"], 
            x0_rand_scale=chain_params["gp_train_params"]["x0_rand_scale"]
        )
    elif chain_params["gp_train_params"]["solver"] == "zoro":
        zoro_solver = ZoroAcados(
            ocp, 
            sim, 
            chain_params["prob_x"], 
            chain_params_aux["Sigma_x0"], 
            chain_params_aux["Sigma_W"], 
            B=chain_params_aux["B"],
            gp_model=gp_model
        )

        # generate train data
        x_train, _ = generate_train_inputs_zoro(
            zoro_solver, 
            chain_params_aux["x0"], 
            chain_params["gp_train_params"]["N_sim_per_x0"], 
            chain_params["gp_train_params"]["N_x0"], 
            random_seed=chain_params["gp_train_params"]["random_seed"], 
            x0_rand_scale=chain_params["gp_train_params"]["x0_rand_scale"]
        )

        # TODO: pre-process data? Subsampling?

        y_train = generate_train_outputs_at_inputs(
            x_train, 
            acados_integrator, 
            acados_integrator_actual, 
            chain_params_aux["Sigma_W"], 
            B=chain_params_aux["B"]
        )

    return x_train, y_train


def get_gp_model_from_chain_params(chain_params, chain_params_aux=None):
    # auxiliary stuff
    if chain_params_aux is None:
        chain_params_aux = get_chain_params_utilities(chain_params)

    # get train data
    if chain_params["gp_train_params"]["N_x0"] > 0:
        x_train, y_train = generate_train_data_from_chain_params(
            chain_params, 
            chain_params_aux=chain_params_aux
        )
        x_train_tensor = Tensor(x_train)
        y_train_tensor = Tensor(y_train)
    else:
        x_train_tensor = torch.empty((1,chain_params_aux["nx"]+chain_params_aux["nu"]))
        y_train_tensor = torch.empty((1,chain_params_aux["nparam"]))

    # initialize model
    likelihood = MultitaskGaussianLikelihood(
        num_tasks = chain_params_aux["nparam"]
    )

    if chain_params["gp_train_params"]["independent_outputs"]:
        gp_model = BatchIndependentMultitaskGPModel(
            x_train_tensor, 
            y_train_tensor, 
            likelihood, 
            chain_params_aux["nparam"]
        )
    else:
        gp_model = MultitaskGPModel(
            x_train_tensor, 
            y_train_tensor, 
            likelihood, 
            chain_params_aux["nparam"], 
            rank = chain_params_aux["nparam"]
        )

    # CUDA?
    if chain_params["gp_train_params"]["use_cuda"] and torch_cuda_is_available():
        x_train_tensor = x_train_tensor.cuda()
        y_train_tensor = y_train_tensor.cuda()
        gp_model = gp_model.cuda()
        likelihood = likelihood.cuda()

    # hyper-parameter optimization
    if chain_params["gp_train_params"]["gp_hyperparams_path"] is not None:
        gp_hyperparams = torch_load(chain_params["gp_train_params"]["gp_hyperparams_path"])
        gp_model.load_state_dict(gp_hyperparams)
        # set train data (needed?)
        # gp_model.set_train_data(
        #     inputs = x_train_tensor, 
        #     targets = y_train_tensor,
        #     strict = True
        # )
    elif chain_params["gp_train_params"]["N_x0"] > 0:
        gp_model, likelihood = train_gp_model(
            gp_model, 
            torch_seed=chain_params["gp_train_params"]["random_seed"], 
            training_iterations=chain_params["gp_train_params"]["train_iter"]
        )

    # eval (posterior) mode
    gp_model.eval()
    likelihood.eval()

    return gp_model, likelihood