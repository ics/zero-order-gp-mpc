# ©2022-​2023 ETH Zurich, Amon Lahr, Andrea Zanelli, Andrea Carron, Melanie N. Zeilinger; D-MAVT; Institute for Dynamical Systems and Control; Intelligent Control Systems Group

import sys, os
sys.path += ["../../src/"]

from acados_template import AcadosSimSolver, AcadosOcpSolver

from export_chain_mass_integrator import export_chain_mass_integrator
from hanging_chain_model_acados import export_ocp
from hanging_chain_gp_utils import *
from utils import *
from plot_utils import *

def run_nominal(chain_params):
    # auxiliary
    chain_params_aux = get_chain_params_utilities(chain_params)
    np.random.seed(chain_params["seed"])
    
    # export ocp without wind consideration
    ocp, sim = export_ocp(
        chain_params, 
        chain_params_aux=chain_params_aux,
        include_wind=False,
        include_covariances_as_params=False
    )
    # set SQP solver type
    ocp.solver_options.nlp_solver_type = 'SQP'
    acados_ocp_solver = AcadosOcpSolver(ocp, json_file = 'acados_ocp_' + ocp.model.name + '.json')

    nx = ocp.model.x.size()[0]
    nu = ocp.model.u.size()[0]

    # closed-loop integrator
    chain_params_cl = chain_params.copy()
    _,sim_cl = export_ocp(
        chain_params_cl,
        include_wind=True
    )
    integrator_cl = AcadosSimSolver(sim_cl)

    # simulate
    N_sim = int(np.floor(chain_params["Tsim"]/chain_params["Ts"]))
    X_sim = np.zeros((N_sim+1,nx))
    U_sim = np.zeros((N_sim,nu))
    # solve_data = []
    timings = np.zeros((N_sim,))
    n_iter = np.zeros((N_sim,))

    # initial condition automatically?
    X_sim[0,:] = chain_params_aux["x0"]
    for i in range(N_sim):
        # set lbx, ubx
        acados_ocp_solver.set(0,"lbx",X_sim[i,:])
        acados_ocp_solver.set(0,"ubx",X_sim[i,:])

        # solve
        acados_ocp_solver.solve()
        # solve_data += [zoro_solver.get_solve_stats()]
        u = acados_ocp_solver.get(0, "u")
        U_sim[i,:] = u

        # simulate
        integrator_cl.set("x", X_sim[i,:])
        integrator_cl.set("u", U_sim[i,:])
        integrator_cl.solve()
        w = np.random.multivariate_normal(
            np.zeros((chain_params_aux["nparam"],)), 
            chain_params_aux["Sigma_W"]
        )
        X_sim[i+1,:] = integrator_cl.get("x") \
            + chain_params_aux["B"] @ w

        timings[i] = acados_ocp_solver.get_stats("time_tot")[0]
        n_iter[i] = acados_ocp_solver.get_stats("sqp_iter")[0]

    # plot results
    if chain_params["show_plots"]:
        plot_chain_control_traj(U_sim)
        plot_chain_position_traj(X_sim, yPosWall=chain_params["yPosWall"])
        plot_chain_velocity_traj(X_sim)
        # plot_chain_position(X_sim[-1,:], xPosFirstMass)
        animate_chain_position(X_sim, chain_params["xPosFirstMass"], yPosWall=chain_params["yPosWall"])
        animate_chain_position_3D(X_sim, chain_params["xPosFirstMass"], Ts=chain_params["Ts"])
        plt.show()

    if chain_params["save_results"]:
        # wall dist
        wall_dist_all = get_wall_dist_from_traj(chain_params, X_sim)
        wall_dist = np.min(wall_dist_all, axis=1)

        ID = "nominal"
        save_closed_loop_results_as_json(ID, n_iter, timings, np.zeros(timings.shape), wall_dist, chain_params)

    return X_sim, U_sim, timings, wall_dist

if __name__ == "__main__":
    # load chain params from cmd argument
    if len(sys.argv) > 1:
        chain_params_file = sys.argv[1]
    
    dir_path = os.path.dirname(os.path.realpath(__file__))
    chain_params_file_abspath = os.path.join(dir_path, chain_params_file)
    
    with open(chain_params_file_abspath, 'r') as f:
        chain_params = json.load(f)

    # execute function
    run_nominal(chain_params)
