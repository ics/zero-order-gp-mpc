# ©2022-​2023 ETH Zurich, Amon Lahr, Andrea Zanelli, Andrea Carron, Melanie N. Zeilinger; D-MAVT; Institute for Dynamical Systems and Control; Intelligent Control Systems Group

import sys, os
# sys.path += ["../../src/"]
dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path += [dir_path]
sys.path += [os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))),"src")]

from run_nominal_control import run_nominal
from run_robust_control import run_robust
from run_tailored_robust_control import run_zoro
from run_tailored_robust_control_with_gp import run_gpzoro

import torch
from torch import save as torch_save
from gp_hyperparam_training import get_prior_covariance
from hanging_chain_gp_utils import get_gp_model_from_chain_params

from utils import *

chain_params = get_chain_params()

gp_hyperparams_path = "gp_hyperparams.pt"

for n_mass in [3,4,5,6,7,8]:
    # for seed in range(10, 51):
    start_iter = True
    for seed in range(0,10):
        for n_x0_train in [100,10,0]:
            for cpu_cores, use_cuda in zip([1, 14, 28, 1],[False, False, False, True]):
                print(f"Starting run with nm={n_mass}, seed={seed}, N_x0={n_x0_train}")
                torch.cuda.empty_cache()

                # adjust parameters wrt experiment
                chain_params["seed"] = seed
                chain_params["n_mass"] = n_mass
                chain_params["gp_train_params"]["random_seed"] = seed
                chain_params["gp_train_params"]["N_x0"] = n_x0_train
                chain_params["gp_train_params"]["use_cuda"] = use_cuda
                chain_params["gp_train_params"]["cpu_cores"] = cpu_cores

                if start_iter:
                    # train gp hyper params with most data
                    chain_params["gp_train_params"]["gp_hyperparams_path"] = None
                    gp_model, likelihood = get_gp_model_from_chain_params(
                        chain_params
                    )

                    if chain_params["gp_train_params"]["use_cuda"] and torch.cuda.is_available():
                        gp_model = gp_model.cuda()
                        likelihood = likelihood.cuda()
                        
                    # save hyper-params
                    torch_save(gp_model.state_dict(), gp_hyperparams_path)
                    chain_params["gp_train_params"]["gp_hyperparams_path"] = gp_hyperparams_path

                    start_iter = False
                
                chain_params_gpprior = chain_params.copy()
                chain_params_gpprior["Sigma_W_add"] = get_prior_covariance(gp_model)

                # save chain params as json
                chain_params_path = "chain_params.json"
                chain_params_gpprior_path = "chain_params_gpprior.json"
                with open(os.path.join(dir_path, chain_params_path), 'w') as f:
                    json.dump(chain_params, f, default=np_array_to_list, indent=4, sort_keys=True)
                with open(os.path.join(dir_path, chain_params_gpprior_path), 'w') as f:
                    json.dump(chain_params_gpprior, f, default=np_array_to_list, indent=4, sort_keys=True)
                    
                if n_x0_train > 0:
                    # run_gpzoro(chain_params)
                    os.system(f"python run_tailored_robust_control_with_gp.py {chain_params_path}")
                else:
                    
                    # run_gpzoro(chain_params_gpprior)
                    os.system(f"python run_tailored_robust_control_with_gp.py {chain_params_gpprior_path}")

        # run all versions
        # run_nominal(chain_params)
        os.system(f"python run_nominal_control.py {chain_params_path}")
        # run_robust(chain_params_gpprior)
        os.system(f"python run_robust_control.py {chain_params_gpprior_path}")

        # remove generated code
        os.system("rm -r c_generated_code/*")
        os.system("rm ./acados_*.json")
        os.system("rm ./jit_*.c")
        os.system("rm ./tmp_casadi_*")