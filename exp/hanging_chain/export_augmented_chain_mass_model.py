# ©2022-​2023 ETH Zurich, Amon Lahr, Andrea Zanelli, Andrea Carron, Melanie N. Zeilinger; D-MAVT; Institute for Dynamical Systems and Control; Intelligent Control Systems Group

from acados_template import AcadosModel
from casadi import SX, vertcat, sin, cos, norm_2, Function, transpose, jacobian

import numpy as np

from export_disturbed_chain_mass_model import export_disturbed_chain_mass_model

from utils import *

def export_augmented_chain_mass_model(n_mass, m, D, L, W, wind_amp=0.0):

    model_name = f"chain_mass_aug_{n_mass}_wind_{str(wind_amp).replace('.','o').replace('-','neg')}"
    og = export_disturbed_chain_mass_model(n_mass, m, D, L,wind_amp=wind_amp)
    nx = og.x.size()[0]

    aug_model = AcadosModel()

    P = SX.sym('P_vec', int((nx+1)*nx/2))
    Pdot = SX.sym('Pdot_vec', int((nx+1)*nx/2))

    x_aug = vertcat(og.x, P)

    A_fun = Function('A_fun', [og.x, og.u, og.p], [jacobian(og.f_expl_expr, og.x)])
    A = A_fun(og.x, og.u, 0)

    # NOTE: need disturbances og.p here, but we eliminate the disturbances from the model later.
    B_fun = Function('B_fun', [og.x, og.u, og.p], [jacobian(og.f_expl_expr, og.p)])
    B = B_fun(og.x, og.u, 0)

    P_mat = vec2sym_mat(P, nx)
    Pdot_mat = A @ P_mat + P_mat @ A.T + B @ W @ B.T
    Pdot_expr = sym_mat2vec(Pdot_mat)
    # set up discrete dynamics
    f_expl = Function('f_expl_chain', [og.x, og.u, og.p], [og.f_expl_expr])
    nominal_f_expl_expr = f_expl(og.x, og.u, 0)

    # fill model
    xdot = vertcat(og.xdot, Pdot)
    f_expl_expr = vertcat(nominal_f_expl_expr, Pdot_expr)
    aug_model.f_expl_expr = f_expl_expr
    aug_model.f_impl_expr = f_expl_expr - xdot

    aug_model.xdot = xdot
    aug_model.u = og.u
    aug_model.x = x_aug
    aug_model.name = model_name

    return aug_model

