# ©2022-​2023 ETH Zurich, Amon Lahr, Andrea Zanelli, Andrea Carron, Melanie N. Zeilinger; D-MAVT; Institute for Dynamical Systems and Control; Intelligent Control Systems Group

from acados_template import AcadosModel
from casadi import SX, vertcat, sin, cos, norm_2

from export_chain_mass_model import export_chain_mass_model

import numpy as np

def export_disturbed_chain_mass_model(n_mass, m, D, L, wind_amp=0.0):
    # get original model
    og = export_chain_mass_model(n_mass, m, D, L, wind_amp=wind_amp)

    model_name = f"chain_mass_ds_{n_mass}_wind_{str(wind_amp).replace('.','_')}"

    # add disturbance
    M = n_mass - 2
    nparam = 3*M
    w = SX.sym("w",nparam,1)

    # dynamics
    f_expl = og.f_expl_expr
    f_expl[-nparam:] += w

    model = AcadosModel()

    model.f_impl_expr = og.xdot - f_expl
    model.f_expl_expr = f_expl
    model.x = og.x
    model.xdot = og.xdot
    model.u = og.u
    model.p = w
    model.name = model_name

    return model

