# ©2022-​2023 ETH Zurich, Amon Lahr, Andrea Zanelli, Andrea Carron, Melanie N. Zeilinger; D-MAVT; Institute for Dynamical Systems and Control; Intelligent Control Systems Group

import sys, os
sys.path += [os.path.dirname(os.path.realpath(__file__))]
sys.path += [os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))),"src")]

from utils import *
import numpy as np
from plot_utils import timings_plot_per_task
from zoro_acados_utils import plot_timings, timings_names_default, timings_names_raw
chain_params = get_chain_params()

timings_names_plot_map = {
    "build_lin_model": "other",
    "query_nodes": "acd_itf",
    "get_gp_sensitivities": "gpytorch",
    "integrate_acados": "acd_itg",
    "integrate_acados_python": None,
    "integrate_get": "acd_itf",
    "integrate_set": "acd_itf",
    "set_sensitivities": "acd_itf",
    "set_sensitivities_reshape": "other",
    "propagate_covar": "con_tight",
    "get_backoffs": None,
    "get_backoffs_htj_sig": "con_tight",
    "get_backoffs_htj_sig_matmul": "con_tight",
    "get_backoffs_add": "con_tight",
    "set_tightening": "acd_itf",
    "phase_one": "other",
    "check_termination": "other",
    "solve_qp": None,
    "solve_qp_acados": "acd_qp",
    "total": None,
}

# timings_names_plot_map = {
#     "query_nodes": "other",
#     "get_gp_sensitivities": "gpytorch",
#     "integrate": None,
#     "integrate_acados": "acados_integrator",
#     "integrate_raw": None,
#     "build_lin_model": "acados_interface",
#     "build_lin_model_python": "numpy",
#     "build_lin_model_raw": None,
#     "set_sensitivities": "acados_interface",
#     "set_sensitivities_reshape": "numpy",
#     "propagate_covar": "propagate",
#     "get_backoffs": None,
#     "get_backoffs_htj_sig": "other",
#     "get_backoffs_htj_sig_matmul": "numpy",
#     "get_backoffs_add": "numpy",
#     "set_tightening": "acados_interface",
#     "phase_one": "other",
#     "check_termination": "other",
#     "solve_qp": None,
#     "solve_qp_acados": "acados_qpsolve"
# }

n_x0_train_all = [0,10,100]

# IDs = ["gp-zoro", "gpu-zoro"]
IDs = ["gp-zoro", "gpu-zoro"]
# Seeds = range(0,10)
Seeds = [0,1,2,3,4,5,6,7,8,9]
N_masses = 7

# mass_dict = {nm: [] for nm in N_masses}
timings = {}
n_iter = {}

for n_x0_train in n_x0_train_all:
    for idk in IDs:
        if idk == "gp-zoro":
            # n_cores_all = [1,10]
            n_cores_all = [1,14]
        else:
            n_cores_all = [1]

        for n_cores in n_cores_all:

        
            if idk == "gp-zoro" or idk == "gpu-zoro":
                idk_save = f'{idk}-x{n_cores}-{n_x0_train*chain_params["gp_train_params"]["N_sim"]}'
                # idk_save = f'{idk}-x{n_cores}'
            elif n_x0_train == 0:
                idk_save=idk
            else:
                continue

            timings[idk_save] = {}
            n_iter[idk_save] = {}

            for seed in Seeds:

                chain_params["seed"] = seed
                chain_params["n_mass"] = N_masses
                chain_params["gp_train_params"]["N_x0"] = n_x0_train
                chain_params["gp_train_params"]["cpu_cores"] = n_cores

                results = load_results_from_json(idk, chain_params)

                for t_key_orig in results["timings_per_task"]:
                    if not any([t_key_orig == key for key in timings_names_plot_map]):
                        t_key = t_key_orig
                    else:
                        t_key = timings_names_plot_map[t_key_orig]

                    if t_key is None:
                        continue
                    
                    if not any([t_key == key for key in timings[idk_save]]):
                        timings[idk_save][t_key] = 0.0

                    timings_per_iter = np.array(results["timings_per_task"][t_key_orig]) / np.array(results["n_iter"])
                    timings[idk_save][t_key] += np.mean(timings_per_iter) / len(Seeds)

# plot
# timings_plot_per_task(timings, timings_names_raw)
task_label_map = {
    "gp-zoro-x1-0": "cpu1", #"alg2-x1",
    "gp-zoro-x14-0": "cpu14", #"alg2-x14",
    "gpu-zoro-x1-0": "gpu", #"alg2-gpu",
    "gp-zoro-x1-150": "cpu1", #"alg2-x1",
    "gp-zoro-x14-150": "cpu14", #"alg2-x14",
    "gpu-zoro-x1-150": "gpu", #"alg2-gpu",
    "gp-zoro-x1-1500": "cpu1", #"alg2-x1",
    "gp-zoro-x14-1500": "cpu14", #"alg2-x14",
    "gpu-zoro-x1-1500": "gpu", #"alg2-gpu",
}

timings_plot_per_task(timings, n_groups=3, group_headers=["$D=0$","$D=150$","$D=1500$"], task_label_map=task_label_map)
# timings_plot_per_task(timings, n_groups=3, group_headers=["$D=0$","$D=150$","$D=1500$"], group_int_labels=["alg2-x1","alg2-x14","alg2-gpu"])

# diagnostics
# for n_x0_train in [50,10,0]:
#     for t_key in results["timings_per_task"]:

#         if idk == "gp-zoro" or idk == "gpu-zoro":
#             idk_save = f'{idk}-x{n_cores}-{n_x0_train*chain_params["gp_train_params"]["N_sim"]}'
#         elif n_x0_train == 0:
#             idk_save=idk
#         else:
#             continue
        
#         idk_cpu = f'gp-zoro-x{n_cores}-{n_x0_train*chain_params["gp_train_params"]["N_sim"]}'
#         idk_gpu = f'gpu-zoro-x{n_cores}-{n_x0_train*chain_params["gp_train_params"]["N_sim"]}'
#         t_ratio_gpu_cpu = timings[idk_gpu][t_key] / timings[idk_cpu][t_key]
#         print(f"GPU/CPU ratio {idk_gpu:12s}/{idk_cpu:12s} - {t_key:30s}:{t_ratio_gpu_cpu:2.2f}")
