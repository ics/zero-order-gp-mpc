# ©2022-​2023 ETH Zurich, Amon Lahr, Andrea Zanelli, Andrea Carron, Melanie N. Zeilinger; D-MAVT; Institute for Dynamical Systems and Control; Intelligent Control Systems Group

import sys, os
sys.path += [os.path.dirname(os.path.realpath(__file__))]


from utils import *
import numpy as np
from plot_utils import timings_plot_vary_mass

plot_time_per_qp = True

chain_params = get_chain_params()



IDs = ["nominal", "naive", "gp-zoro", "gpu-zoro"]
IDs_save = ["nominal", "naive", "gp-zoro", "gpu-zoro"]
# Seeds = range(0,10)
Seeds = [0,1,2,3,4,6,7,8,9]
N_masses = range(3,9)
# n_x0_train_arr = [0,10,100]
n_x0_train_arr = [0,100]

# mass_dict = {nm: [] for nm in N_masses}
timings = {}
n_iter = {}

for idk in IDs:
    if idk == "gp-zoro":
        # n_cores_all = [1,10]
        n_cores_all = [1,14]
    else:
        n_cores_all = [1]

    for n_cores in n_cores_all:
        for n_x0_train in n_x0_train_arr:

            if idk == "gp-zoro":
                idk_save = f'alg2-cpu{n_cores}-{n_x0_train*chain_params["gp_train_params"]["N_sim"]}'
            elif idk == "gpu-zoro":
                idk_save = f'alg2-gpu-{n_x0_train*chain_params["gp_train_params"]["N_sim"]}'
            elif n_x0_train == 0:
                idk_save=idk
                if idk == "naive":
                    idk_save = r"na\"ive"
                elif idk == "nominal":
                    idk_save = r"nominal"
            else:
                continue

            # skip d= 0 for gpu
            if idk == "gpu-zoro" and n_x0_train == 0:
                continue

            # skip d= 0 for cpu-x14
            if idk == "gp-zoro" and n_x0_train == 0 and n_cores > 1:
                continue

            timings[idk_save] = {}
            n_iter[idk_save] = {}

            for i_mass, n_mass in enumerate(N_masses):
                for seed in Seeds:
                    
                    timings[idk_save][n_mass] = []
                    n_iter[idk_save][n_mass] = []

                    chain_params["seed"] = seed
                    chain_params["n_mass"] = n_mass
                    chain_params["gp_train_params"]["N_x0"] = n_x0_train
                    chain_params["gp_train_params"]["cpu_cores"] = n_cores

                    results = load_results_from_json(idk, chain_params)
                    total_timing = np.array(results["timings"])

                    if plot_time_per_qp:
                        # print(f"idk: {idk}, nm: {n_mass}, n_iter: {results['n_iter']}")
                        total_iter = np.array(results["n_iter"])
                        n_iter[idk_save][n_mass] = n_iter[idk_save][n_mass] + list(total_iter)

                    timings[idk_save][n_mass] = timings[idk_save][n_mass] + list(total_timing)

# plot
if plot_time_per_qp:
    timings_plot_vary_mass(timings, N_masses, n_iter_all=n_iter)
else:
    timings_plot_vary_mass(timings, N_masses)