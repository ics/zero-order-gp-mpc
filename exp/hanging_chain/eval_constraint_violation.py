# ©2022-​2023 ETH Zurich, Amon Lahr, Andrea Zanelli, Andrea Carron, Melanie N. Zeilinger; D-MAVT; Institute for Dynamical Systems and Control; Intelligent Control Systems Group

import sys, os
sys.path += [os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))),"src")]

from utils import *
import numpy as np
from plot_utils import plot_constraint_violation_cdf

chain_params = get_chain_params()

IDs = ["nominal", "zoro", "robust", "gpzoro"]
Seeds = range(10,15)
# Seeds = range(1,29)
n_mass = 5

dist_dict = {id:[] for id in IDs}

# load results
for id in IDs:
    for seed in Seeds:
        chain_params["seed"] = seed
        chain_params["n_mass"] = n_mass
        results = load_results_from_json(id, chain_params)
        dist_dict[id] += results["wall_dist"]

# plot
plot_constraint_violation_cdf(dist_dict, n_mass, chain_params["prob_x"])

