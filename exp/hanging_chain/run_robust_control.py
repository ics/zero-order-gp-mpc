#
# Copyright 2019 Gianluca Frison, Dimitris Kouzoupis, Robin Verschueren,
# Andrea Zanelli, Niels van Duijkeren, Jonathan Frey, Tommaso Sartor,
# Branimir Novoselnik, Rien Quirynen, Rezart Qelibari, Dang Doan,
# Jonas Koenemann, Yutao Chen, Tobias Schöls, Jonas Schlagenhauf, Moritz Diehl
#
# This file is part of acados.
#
# The 2-Clause BSD License
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.;
#

import sys
import numpy as np
import scipy.linalg
import casadi as ca

from acados_template import AcadosOcp, AcadosOcpSolver, AcadosSimSolver

from export_chain_mass_model import export_chain_mass_model
from export_augmented_chain_mass_model import export_augmented_chain_mass_model
from export_chain_mass_integrator import export_chain_mass_integrator
from hanging_chain_model_acados import export_ocp

from plot_utils import *
from utils import *
import matplotlib.pyplot as plt

def run_robust(chain_params):
    # auxiliary
    chain_params_aux = get_chain_params_utilities(chain_params)
    np.random.seed(chain_params["seed"])

    # export ocp without wind consideration
    ocp, sim = export_ocp(
        chain_params, 
        chain_params_aux=chain_params_aux,
        include_wind=False,
        include_covariances=True
    )
    # set SQP solver type
    ocp.solver_options.nlp_solver_type = 'SQP'
    acados_ocp_solver = AcadosOcpSolver(ocp, json_file = 'acados_ocp_' + ocp.model.name + '.json')

    M = chain_params_aux["M"]
    nx_nom = M * 3 + (M+1)*3
    nx = ocp.model.x.size()[0]
    nu = ocp.model.u.size()[0]

    # closed-loop integrator
    chain_params_cl = chain_params.copy()
    _,sim_cl = export_ocp(
        chain_params_cl,
        include_wind=True,
        include_covariances=False
    )
    integrator_cl = AcadosSimSolver(sim_cl)

    # simulate
    N_sim = int(np.floor(chain_params["Tsim"]/chain_params["Ts"]))
    X_sim = np.zeros((N_sim+1,nx_nom))
    U_sim = np.zeros((N_sim,nu))
    # solve_data = []
    timings = np.zeros((N_sim,))
    n_iter = np.zeros((N_sim,))

    # initial condition automatically?
    X_sim[0,:] = ocp.constraints.lbx_0[:nx_nom]
    P0_vec = ocp.constraints.lbx_0[nx_nom:]
    for i in range(N_sim):
        # set lbx, ubx
        xcurrent = np.hstack((
            X_sim[i,:], 
            P0_vec
        ))
        acados_ocp_solver.set(0,"lbx",xcurrent)
        acados_ocp_solver.set(0,"ubx",xcurrent)

        # solve
        acados_ocp_solver.solve()
        # solve_data += [zoro_solver.get_solve_stats()]
        u = acados_ocp_solver.get(0, "u")
        U_sim[i,:] = u

        # simulate
        integrator_cl.set("x", X_sim[i,:])
        integrator_cl.set("u", U_sim[i,:])
        integrator_cl.solve()
        w = np.random.multivariate_normal(
            np.zeros((chain_params_aux["nparam"],)), 
            chain_params_aux["Sigma_W"]
        )
        X_sim[i+1,:] = integrator_cl.get("x") \
            + chain_params_aux["B"] @ w

        timings[i] = acados_ocp_solver.get_stats("time_tot")[0]
        n_iter[i] = acados_ocp_solver.get_stats("sqp_iter")[0]

    # plot results
    if chain_params["show_plots"]:
        plot_chain_control_traj(U_sim)
        plot_chain_position_traj(X_sim, yPosWall=chain_params["yPosWall"])
        plot_chain_velocity_traj(X_sim)
        # plot_chain_position(X_sim[-1,:], xPosFirstMass)
        animate_chain_position(X_sim, chain_params["xPosFirstMass"], yPosWall=chain_params["yPosWall"])
        animate_chain_position_3D(X_sim, chain_params["xPosFirstMass"], Ts=chain_params["Ts"])
        plt.show()

    if chain_params["save_results"]:
        # wall dist
        wall_dist_all = get_wall_dist_from_traj(chain_params, X_sim)
        wall_dist = np.min(wall_dist_all, axis=1)

        ID = "naive"
        save_closed_loop_results_as_json(ID, n_iter, timings, np.zeros(timings.shape), wall_dist, chain_params)

    return X_sim, U_sim, timings, wall_dist

if __name__ == "__main__":
    # load chain params from cmd argument
    if len(sys.argv) > 1:
        chain_params_file = sys.argv[1]
    
    dir_path = os.path.dirname(os.path.realpath(__file__))
    chain_params_file_abspath = os.path.join(dir_path, chain_params_file)
    
    with open(chain_params_file_abspath, 'r') as f:
        chain_params = json.load(f)

    # execute function
    run_robust(chain_params)