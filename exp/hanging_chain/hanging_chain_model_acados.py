# ©2022-​2023 ETH Zurich, Amon Lahr, Andrea Zanelli, Andrea Carron, Melanie N. Zeilinger; D-MAVT; Institute for Dynamical Systems and Control; Intelligent Control Systems Group

import numpy as np
from acados_template import AcadosOcp #, AcadosOcpSolver, AcadosSimSolver
from casadi import vertcat
from scipy.stats import norm

from export_chain_mass_model import export_chain_mass_model
from export_augmented_chain_mass_model import export_augmented_chain_mass_model
# from export_disturbed_chain_mass_model import export_disturbed_chain_mass_model
from export_chain_mass_integrator import export_chain_mass_integrator

from plot_utils import *
from utils import *
import matplotlib.pyplot as plt

def export_ocp(
    chain_params, 
    chain_params_aux=None, 
    include_wind=False, 
    include_covariances=False, 
    include_covariances_as_params=False
):
    # auxiliary
    if chain_params_aux is None:
        chain_params_aux = get_chain_params_utilities(chain_params)

    # create ocp object to formulate the OCP
    ocp = AcadosOcp()

    # chain parameters
    n_mass = chain_params["n_mass"]
    M = chain_params["n_mass"] - 2 # number of intermediate masses
    Ts = chain_params["Ts"]
    Tsim = chain_params["Tsim"]
    N = chain_params["N"]
    u_init = chain_params["u_init"]
    with_wall = chain_params["with_wall"]
    yPosWall = chain_params["yPosWall"]
    m = chain_params["m"]
    D = chain_params["D"]
    L = chain_params["L"]
    perturb_scale = chain_params["perturb_scale"]
    xPosFirstMass = chain_params["xPosFirstMass"]

    if include_wind:
        wind_amp = chain_params["wind_amp"] 
    else:
        wind_amp = 0.0

    nlp_iter = chain_params["nlp_iter"]
    nlp_tol = chain_params["nlp_tol"]
    save_results = chain_params["save_results"]
    show_plots = chain_params["show_plots"]
    seed = chain_params["seed"]

    # auxiliary params
    xrest = chain_params_aux["xrest"]

    # export model
    if include_covariances:
        W_add = chain_params_aux["Sigma_W_add"]
        W = chain_params_aux["Sigma_W"]

        model_sim = export_augmented_chain_mass_model(n_mass, m, D, L, W+W_add, wind_amp=wind_amp)
        model_ocp = model_sim
        _, sim = export_chain_mass_integrator(model_sim, Ts)

        nx_nom = M * 3 + (M+1)*3
        nx = model_ocp.x.size()[0]
    else:
        model_sim = export_chain_mass_model(n_mass, m, D, L, wind_amp=wind_amp, json_prefix="sim")
        model_ocp = export_chain_mass_model(n_mass, m, D, L, wind_amp=wind_amp, json_prefix="ocp")
        _, sim = export_chain_mass_integrator(model_sim, Ts)

        nx_nom = model_ocp.x.size()[0]
        nx = nx_nom

    # set model
    ocp.model = model_ocp

    nu = model_ocp.u.size()[0]
    ny = nx_nom + nu
    ny_e = nx_nom
    Tf = N * Ts

    # set dimensions
    ocp.dims.N = N

    # set cost module
    ocp.cost.cost_type = 'LINEAR_LS'
    ocp.cost.cost_type_e = 'LINEAR_LS'

    Q = get_Q(chain_params)
    R = get_R(chain_params)

    ocp.cost.W = scipy.linalg.block_diag(Q, R)
    ocp.cost.W_e = Q

    ocp.cost.Vx = np.zeros((ny, nx))
    ocp.cost.Vx[:nx_nom,:nx_nom] = np.eye(nx_nom)

    Vu = np.zeros((ny, nu))
    Vu[nx_nom:nx_nom+nu, :] = np.eye(nu)
    ocp.cost.Vu = Vu

    Vx_e = np.zeros((ny_e, nx))
    Vx_e[:nx_nom, :nx_nom] = np.eye(nx_nom)
    ocp.cost.Vx_e = Vx_e

    # import pdb; pdb.set_trace()
    yref = np.vstack((xrest, np.zeros((nu,1)))).flatten()
    ocp.cost.yref = yref
    ocp.cost.yref_e = xrest.flatten()

    # set constraints
    umax = 1*np.ones((nu,))

    ocp.constraints.constr_type = 'BGH'
    ocp.constraints.lbu = -umax
    ocp.constraints.ubu = umax
    ocp.constraints.idxbu = np.array(range(nu))

    # initial condition
    if include_covariances:
        P0_vec = sym_mat2vec(chain_params_aux["Sigma_x0"])
        ocp.constraints.x0 = np.hstack((
            chain_params_aux["x0"], 
            P0_vec
        ))
    else:
        ocp.constraints.x0 = chain_params_aux["x0"]


    # disturbances
    # ocp.parameter_values = np.zeros((nparam,))

    prob_tighten = norm.ppf(chain_params["prob_x"])
    # wall constraint
    if with_wall:
        nh = M + 1
        con_h_expr = ca.SX.zeros(nh,1)
        
        if include_covariances:
            P_mat = vec2sym_mat( model_ocp.x[nx_nom:], nx_nom )
        elif include_covariances_as_params:
            P_vec = ca.SX.sym('P_vec', int((nx_nom+1)*nx_nom/2))
            P_mat = vec2sym_mat(P_vec, nx_nom)
            ocp.model.p = ca.vertcat(ocp.model.p, P_vec)
            # ocp.parameter_values = np.zeros((nparam,))
        
        if include_covariances or include_covariances_as_params:
            for j in range(nh):
                # Note: lower bound, therefore need to substract the backoff term
                # TODO: use general constraint tightening function?
                con_h_expr[j] = model_ocp.x[3*j+1] - prob_tighten * ca.sqrt(P_mat[3*j+1,3*j+1])
        else:
            for j in range(nh):
                con_h_expr[j] = model_ocp.x[3*j+1]

        # ocp.constraints.Jbx = Jbx
        # ocp.constraints.lbx = yPosWall * np.ones((nh,))
        # ocp.constraints.ubx = 1e9 * np.ones((nh,))
        
        # model.con_h_expr = vertcat(*con_h_expr)
        ocp.model.con_h_expr = con_h_expr
        ocp.constraints.lh = yPosWall * np.ones((nh,))
        ocp.constraints.uh = 1e9 * np.ones((nh,))

        # slacks
        # ocp.constraints.Jsbx = np.eye(nh)
        ocp.constraints.Jsh = np.eye(nh)
        L2_pen = 1e3
        L1_pen = 1
        ocp.cost.Zl = L2_pen * np.ones((nh,))
        ocp.cost.Zu = L2_pen * np.ones((nh,))
        ocp.cost.zl = L1_pen * np.ones((nh,))
        ocp.cost.zu = L1_pen * np.ones((nh,))


    # solver options
    ocp.solver_options.qp_solver = 'PARTIAL_CONDENSING_HPIPM' # FULL_CONDENSING_QPOASES
    ocp.solver_options.hessian_approx = 'GAUSS_NEWTON'
    ocp.solver_options.integrator_type = 'IRK'

    if include_covariances:
        ocp.solver_options.nlp_solver_type = 'SQP' # SQP, SQP_RTI
    else:
        ocp.solver_options.nlp_solver_type = 'SQP_RTI' # SQP, SQP_RTI

    ocp.solver_options.sim_method_num_stages = 2
    ocp.solver_options.sim_method_num_steps = 1 # 2
    ocp.solver_options.qp_solver_cond_N = N
    ocp.solver_options.tol = nlp_tol
    ocp.solver_options.qp_tol = nlp_tol
    ocp.solver_options.nlp_solver_max_iter = nlp_iter

    # amon opts
    ocp.solver_options.qp_solver_warm_start = 1

    # ocp.solver_options.qp_solver_iter_max = 500
    # ocp.solver_options.initialize_t_slacks = 1
    # ocp.solver_options.qp_solver_max_iter = 100

    # set prediction horizon
    ocp.solver_options.tf = Tf

    return ocp, sim

def get_Q(chain_params):
    # nx = (3+3)*(nm-1)-3
    M = chain_params["n_mass"] - 2
    nx = 6 * M + 3
    q_diag = np.ones((nx,1))
    strong_penalty = M+1
    q_diag[3*M] = strong_penalty
    q_diag[3*M+1] = strong_penalty
    q_diag[3*M+2] = strong_penalty
    Q = chain_params["cost_fac_Q"] * np.diagflat( q_diag )
    return Q

def get_R(chain_params):
    nu = 3
    R = chain_params["cost_fac_R"] * np.diagflat( np.ones((nu, 1)) )
    return R

