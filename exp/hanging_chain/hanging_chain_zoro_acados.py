# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.1
#   kernelspec:
#     display_name: Python 3.9.13 ('zero-order-gp-mpc-code-2CX1fffa')
#     language: python
#     name: python3
# ---

# ©2022-​2023 ETH Zurich, Amon Lahr, Andrea Zanelli, Andrea Carron, Melanie N. Zeilinger; D-MAVT; Institute for Dynamical Systems and Control; Intelligent Control Systems Group

# %%
import sys, os
sys.path += ["../../src/"]

import hanging_chain_model_acados
import torch
import importlib
import cProfile

importlib.reload(hanging_chain_model_acados)

from export_chain_mass_integrator import export_chain_mass_integrator
from hanging_chain_model_acados import export_ocp, get_Q, get_R
from utils import *
from plot_utils import *

from zoro_acados import ZoroAcados

# %%
chain_params = get_chain_params()
chain_params["n_mass"] = 7
chain_params["perturb_scale"] = 1e-1
chain_params["wind_amp"] = -0.1
chain_params["seed"] = 5
chain_params["gp_train_params"]["random_seed"] = chain_params["seed"]
chain_params["gp_train_params"]["use_cuda"] = True
chain_params["gp_train_params"]["N_x0"] = 1
chain_params["gp_train_params"]["train_iter"] = 100

chain_params_aux = get_chain_params_utilities(chain_params)
M = chain_params_aux["M"]
nparam = chain_params_aux["nparam"]
Sigma_W = chain_params_aux["Sigma_W"]

chain_params, chain_params_aux

# %%
if chain_params["gp_train_params"]["use_cuda"]:
    torch.cuda.empty_cache()
    to_numpy = lambda T: T.cpu().numpy()
else:
    to_numpy = lambda T: T.numpy()

chain_params["gp_train_params"]["use_cuda"]

# %%
ocp, sim = export_ocp(chain_params, chain_params_aux=chain_params_aux, include_wind=False, include_covariances_as_params=True)

nx = ocp.model.x.size()[0]
nu = ocp.model.u.size()[0]

# %%
sim.model.p

# %%
ocp.model.p.shape

# %%
ocp.code_export_directory

# %%
prob_x = 0.95
Sigma_x0 = 1e-6*np.eye(nx)

B = np.vstack(( np.zeros((nx - nparam, nparam)), np.eye(nparam)))
zoro_solver = ZoroAcados(ocp, sim, prob_x, Sigma_x0, Sigma_W, B=B)

# zoro_solver.ocp_solver.set(0, "lbx", xcurrent)
# zoro_solver.ocp_solver.set(0, "ubx", xcurrent)

# %%
# for i in range(N):
#     zoro_solver.ocp_solver.set(i, "x",X_init[i,:])
#     zoro_solver.ocp_solver.set(i, "u",U_init[i,:])
# zoro_solver.ocp_solver.set(N, "x",X_init[N,:])

zoro_solver.solve(tol_nlp=chain_params["nlp_tol"])

# %%
zoro_solver.print_solve_stats()

# %%
# import casadi as cas

# hjf = zoro_solver.h_jac_fun
# hjf.sparsity_in(0)

# hjf.generate("hjf.c")
# hjf.jit("hjf.c", )

# %%
chain_params

# %%
X,U,P = zoro_solver.get_solution()

plot_chain_position_traj(X, yPosWall=chain_params["yPosWall"])

# %%
plot_chain_velocity_traj(X)

# %%
plot_chain_control_traj(U)


# %%
# plot_chain_position_3D(x0, xPosFirstMass, XNames=None)

# %%
ani = animate_chain_position_3D(X, chain_params["xPosFirstMass"], Ts=0.1)

# %%
torch.Tensor(np.array([]))

# %%
torch.Tensor(np.array([[]*3])).shape[1]

# %%
ani.save("test.gif")

# %%
xEndRef = chain_params["L"] * (M+1) *6
t = np.linspace(0,xEndRef,1000)
amp1 = 1
amp2 = 1
w1 = 1
w2 = 1.5
k1 = w1 * 2 * np.pi / xEndRef
k2 = w2 * 2 * np.pi / xEndRef

f_w = amp1 * np.sin(k1 * t) + amp2 * np.sin(k2 *t)**2
plt.plot(t,f_w)

# %%
# acados_integrator,_ = export_chain_mass_integrator(
#     chain_params["n_mass"],
#     chain_params["m"],
#     chain_params["D"],
#     chain_params["L"]
# )

# %%
# -------------------- HOW TO SET HYPER PARAMS -----------------------

# set task covariance to identity
# gp_model.likelihood.raw_noise.requires_grad = False
# set_gp_param_value(gp_model, "likelihood.raw_noise", torch.Tensor([w_theta]))

# gp_model.covar_module.task_covar_module.covar_factor.requires_grad = False
# set_gp_param_value(gp_model, "covar_module.task_covar_module.covar_factor", torch.Tensor(np.eye(nx)))

# # gp_model.covar_module.task_covar_module.raw_var.requires_grad = False
# # set_gp_param_value(gp_model, "covar_module.task_covar_module.raw_var", torch.Tensor(np.array([0.1, 0.1])))
# gp_model.covar_module.data_covar_module.raw_lengthscale.requires_grad = False
# set_gp_param_value(gp_model, "covar_module.data_covar_module.raw_lengthscale", torch.Tensor(np.array([[1, 1]])))

# gp_model.covar_module.data_covar_module.register_constraint("raw_lengthscale", gpytorch.constraints.Interval(0,10))
# gp_model.covar_module.data_covar_module.register_constraint("raw_lengthscale", gpytorch.constraints.GreaterThan(0))


# %%
import torch
import hanging_chain_gp_utils
importlib.reload(hanging_chain_model_acados)
from hanging_chain_gp_utils import get_gp_model_from_chain_params

gp_model, likelihood = get_gp_model_from_chain_params(chain_params)


# %%
torch.empty((1,3))

# %%
gp_model.train_inputs[0]

# %%
gp_model.train_targets.shape

# %%

if chain_params["gp_train_params"]["use_cuda"] and torch.cuda.is_available():
    gp_model = gp_model.cuda()
    likelihood = likelihood.cuda()

# %%
import torch
batch_shape=torch.Size([2])
batch_shape

# %%
torch.cuda.empty_cache()

# %%
import gp_utils
importlib.reload(gp_utils)
from gp_utils import gp_data_from_model_and_path, gp_derivative_data_from_model_and_path, plot_gp_data, generate_grid_points

num_samples = 5
use_likelihood = False

num_points_between_samples = 30
t_lin = np.linspace(0,1,num_points_between_samples,endpoint=False)

x_plot_waypts = np.hstack((
    X[1:,:],
    U
)) 
x_plot = []
for i in range(x_plot_waypts.shape[0]-1):
    x_plot += [x_plot_waypts[i,:] + (x_plot_waypts[i+1,:] - x_plot_waypts[i,:]) * t for t in t_lin]
x_plot = np.vstack(zip(x_plot))

gp_data = gp_data_from_model_and_path(gp_model, likelihood, x_plot, num_samples=num_samples, use_likelihood=use_likelihood)
plot_gp_data([gp_data], marker_size_lim=[1, 15])

# %%
# gp_derivative_data = gp_derivative_data_from_model_and_path(gp_model, likelihood, x_plot, num_samples=0)
# plot_gp_data([gp_derivative_data], marker_size_lim=[5, 20], plot_train_data=False)

# %%
ocp, sim = export_ocp(chain_params, chain_params_aux=chain_params_aux, include_wind=False, include_covariances_as_params=True)
zoro_solver_gp = ZoroAcados(ocp, sim, prob_x, Sigma_x0, Sigma_W, B=B, gp_model=gp_model)

# %%
zoro_solver_gp.solve(tol_nlp=chain_params["nlp_tol"])

# %%
X,U,P = zoro_solver_gp.get_solution()

plot_chain_position_traj(X, yPosWall=chain_params["yPosWall"])

# %%
zoro_solver_gp.print_solve_stats()

# %%
# get GP covar and set zoro solver to GP value
import torch

gp_model.eval()
y_test = torch.Tensor(np.ones((1,nx+nu)))

Sigma_GP_prior = np.diag(to_numpy(gp_model.covar_module(y_test)).flatten())
Sigma_GP_prior, Sigma_W

# %%
Sigma_GP_prior

# %%
ocp, sim = export_ocp(chain_params, chain_params_aux=chain_params_aux, include_wind=False, include_covariances_as_params=True)
# zoro_solver_gp_prior = ZoroAcados(ocp, sim, prob_x, Sigma_x0, Sigma_W+Sigma_GP_prior, B=B)
zoro_solver_gp_prior = ZoroAcados(ocp, sim, prob_x, Sigma_x0, Sigma_W+Sigma_GP_prior, B=B)
zoro_solver_gp_prior.solve(tol_nlp=chain_params["nlp_tol"])


# %%
zoro_solver_gp_prior.print_solve_stats()


# %%
X,U,P = zoro_solver_gp_prior.get_solution()

plot_chain_position_traj(X, yPosWall=chain_params["yPosWall"])

# %%
from acados_template import AcadosSimSolver

n_sim = 50

X_sim = np.zeros((n_sim+1,nx))
U_sim = np.zeros((n_sim,nu))
solve_data_gpprior = []

chain_params_cl = chain_params.copy()

_,sim_cl = export_ocp(chain_params_cl)
integrator_cl = AcadosSimSolver(sim_cl)
# initial condition automatically?
X_sim[0,:] = X[0,:]
for i in range(n_sim):
    # set lbx, ubx
    zoro_solver_gp_prior.ocp_solver.set(0,"lbx",X_sim[i,:])
    zoro_solver_gp_prior.ocp_solver.set(0,"ubx",X_sim[i,:])

    # solve
    zoro_solver_gp_prior.solve(tol_nlp=1e-5)
    solve_data_gpprior += [zoro_solver_gp_prior.get_solve_stats()]
    _,U,_ = zoro_solver_gp_prior.get_solution()
    U_sim[i,:] = U[0,:]

    # simulate
    integrator_cl.set("x", X_sim[i,:])
    integrator_cl.set("u", U_sim[i,:])
    integrator_cl.solve()
    X_sim[i+1,:] = integrator_cl.get("x") + B @ np.random.multivariate_normal(np.zeros((nparam,)), Sigma_W)

# %%
import zoro_acados_utils
importlib.reload(zoro_acados_utils)
from zoro_acados_utils import plot_timings, timings_names_default, timings_names_raw, timings_names_backoffs

plot_timings(solve_data_gpprior, timings_names=timings_names_default)
plot_timings(solve_data_gpprior, timings_names=timings_names_raw)
plot_timings(solve_data_gpprior, timings_names=timings_names_backoffs)

# %%
zoro_solver_gp_prior

# %%
from acados_template import AcadosSimSolver

n_sim = 50

X_sim = np.zeros((n_sim+1,nx))
U_sim = np.zeros((n_sim,nu))
solve_data = []

chain_params_cl = chain_params.copy()

_,sim_cl = export_ocp(chain_params_cl)
integrator_cl = AcadosSimSolver(sim_cl)
# initial condition automatically?
X_sim[0,:] = X[0,:]
for i in range(n_sim):
    # set lbx, ubx
    zoro_solver.ocp_solver.set(0,"lbx",X_sim[i,:])
    zoro_solver.ocp_solver.set(0,"ubx",X_sim[i,:])

    # solve
    zoro_solver.solve(tol_nlp=1e-5)
    solve_data += [zoro_solver.get_solve_stats()]
    _,U,_ = zoro_solver.get_solution()
    U_sim[i,:] = U[0,:]

    # simulate
    integrator_cl.set("x", X_sim[i,:])
    integrator_cl.set("u", U_sim[i,:])
    integrator_cl.solve()
    X_sim[i+1,:] = integrator_cl.get("x") + B @ np.random.multivariate_normal(np.zeros((nparam,)), Sigma_W)

# %%
plot_timings(solve_data)

# %%
zoro_solver.print_solve_stats()

# %%
zoro_solver_gp.gp_model.train_inputs[0].shape

# %%
if chain_params["gp_train_params"]["use_cuda"] and torch.cuda.is_available():
    gp_model = gp_model.cuda()
    likelihood = likelihood.cuda()

# %%
from acados_template import AcadosSimSolver

n_sim = 50

X_sim = np.zeros((n_sim+1,nx))
U_sim = np.zeros((n_sim,nu))
solve_data_gp = []

chain_params_cl = chain_params.copy()

_,sim_cl = export_ocp(chain_params_cl)
integrator_cl = AcadosSimSolver(sim_cl)
# initial condition automatically?
X_sim[0,:] = X[0,:]
for i in range(n_sim):
    # set lbx, ubx
    zoro_solver_gp.ocp_solver.set(0,"lbx",X_sim[i,:])
    zoro_solver_gp.ocp_solver.set(0,"ubx",X_sim[i,:])

    # solve
    # cProfile.run("zoro_solver_gp.solve(tol_nlp=1e-5)")
    zoro_solver_gp.solve(tol_nlp=5e-5)
    solve_data_gp += [zoro_solver_gp.get_solve_stats()]
    _,U,_ = zoro_solver_gp.get_solution()
    U_sim[i,:] = U[0,:]

    # simulate
    integrator_cl.set("x", X_sim[i,:])
    integrator_cl.set("u", U_sim[i,:])
    integrator_cl.solve()
    X_sim[i+1,:] = integrator_cl.get("x") + B @ np.random.multivariate_normal(np.zeros((nparam,)), Sigma_W)

# %%
from zoro_acados_utils import plot_timings, timings_names_raw
plot_timings(solve_data_gp)

# %%
plot_timings(solve_data_gp, timings_names_raw)

# %%
# find number of constraint violations
dist_wall = X_sim[:,range(1,3*M+2,3)]
fig, ax = plt.subplots(figsize=(8,3))
n_bins = 100
n, bins, patches = ax.hist(dist_wall[:,0],n_bins,cumulative=1,density=True)
ax.plot(bins[0:-1], n, 'k--')

# %%
plot_chain_position_traj(X_sim, yPosWall=chain_params["yPosWall"])


# %%
plot_chain_control_traj(U_sim)


# %%
ani = animate_chain_position_3D(X_sim, chain_params["xPosFirstMass"], Ts=0.1)
ani.save("test_cl.gif")

# %%
solve_data[0].timings["total"]

# %%
t_total = []
n_iter = []
t_iter = []
t_iter_avg = []
for s in solve_data:
    t_iter_arr = s.timings["total"]
    t_iter_avg += [np.sum(t_iter_arr) / s.n_iter]
    t_iter += [t_iter_arr]
    t_total += [s.timings_total]
    n_iter += [s.n_iter]

t_total, n_iter, t_iter


# %%
t_iter_avg

# %%
# get closed-loop discrete cost
chain_params["cost_fac_Q"] = 2
Q = get_Q(chain_params)
Qtest = ocp.cost.W[0:nx,0:nx]

np.linalg.norm(Q-Qtest)

# %%
chain_params["cost_fac_R"] = 2e-2
R = get_R(chain_params)
Rtest = ocp.cost.W[nx:nx+nu,nx:nx+nu]

np.linalg.norm(R-Rtest)

# %%
R-Rtest

# %%
ocp.cost.yref

# %%
cost = 0
for i in range(n_sim):
    xd = ocp.cost.yref[0:nx] - X_sim[i,:]
    ud = ocp.cost.yref[nx:nx+nu] - U_sim[i,:]
    cost += xd.T @ Q @ xd + ud.T @ R @ ud
cost += X_sim[n_sim,:].T @ Q @ X_sim[n_sim,:]

cost

# %%
ocp.cost.yref[0:nx] - X_sim[i,:]

# %%
for a,b in zip([1,2],[3,4]): 
    print(a)
    print(b)

# %%
