# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.1
#   kernelspec:
#     display_name: Python 3.9.13 ('zero-order-gp-mpc-code-2CX1fffa')
#     language: python
#     name: python3
# ---

# ©2022-​2023 ETH Zurich, Amon Lahr, Andrea Zanelli, Andrea Carron, Melanie N. Zeilinger; D-MAVT; Institute for Dynamical Systems and Control; Intelligent Control Systems Group

# %%
from run_tailored_robust_control_with_gp import run_gpzoro
from utils import get_chain_params
from hanging_chain_gp_utils import get_gp_model_from_chain_params
from torch import save as torch_save
from gp_hyperparam_training import get_prior_covariance

import importlib
import numpy as np

# %%
chain_params = get_chain_params()

# chain_params["wind_amp"] = -0.4
# chain_params["Tsim"] = 10
# chain_params["perturb_scale"] = 1e-2
chain_params["show_plots"] = True
chain_params["gp_train_params"]["N_x0"] = 1
# chain_params["gp_train_params"]["x0_rand_scale"] = 0.5
# chain_params["gp_train_params"]["N_sim"] = 15

chain_params

# %%
import torch
torch.cuda.empty_cache()

gp_hyperparams_path = "test_hyperparams"

gp_model, likelihood = get_gp_model_from_chain_params(
    chain_params
)
# save hyper-params
torch_save(gp_model.state_dict(), gp_hyperparams_path)
chain_params["gp_train_params"]["gp_hyperparams_path"] = gp_hyperparams_path

chain_params_gpprior = chain_params.copy()
chain_params_gpprior["Sigma_W_add"] = get_prior_covariance(gp_model)

# %%
X,U,solve_data = run_gpzoro(chain_params)

# %%
import gp_utils
importlib.reload(gp_utils)
from gp_utils import gp_data_from_model_and_path, gp_derivative_data_from_model_and_path, plot_gp_data, generate_grid_points

num_samples = 5
use_likelihood = False

num_points_between_samples = 30
t_lin = np.linspace(0,1,num_points_between_samples,endpoint=False)

x_plot_waypts = np.hstack((
    X[1:,:],
    U
)) 
x_plot = []
for i in range(x_plot_waypts.shape[0]-1):
    x_plot += [x_plot_waypts[i,:] + (x_plot_waypts[i+1,:] - x_plot_waypts[i,:]) * t for t in t_lin]
x_plot = np.vstack(zip(x_plot))

gp_data = gp_data_from_model_and_path(gp_model, likelihood, x_plot, num_samples=num_samples, use_likelihood=use_likelihood)
plot_gp_data([gp_data], marker_size_lim=[1, 15])

# %%
import numpy as np
t_total = []
n_iter = []
t_iter = []
t_iter_avg = []

for s in solve_data:
    t_iter_arr = s.timings["total"]
    t_iter_avg += [np.sum(t_iter_arr) / s.n_iter]
    t_iter += [t_iter_arr]
    t_total += [s.timings_total]
    n_iter += [s.n_iter]

# %%
n_iter

# %%
M = chain_params["n_mass"] - 2
np.min(X[:,range(1,3*M+2,3)],axis=1)

# %%
from run_tailored_robust_control import run_zoro

X,U,solve_data = run_zoro(chain_params_gpprior)

# %%
import importlib

import run_nominal_control
importlib.reload(run_nominal_control)
from run_nominal_control import run_nominal

X,U,timings,wall_dist = run_nominal(chain_params_gpprior)

# %%
timings

# %%
wall_dist

# %%
import importlib

import run_robust_control
importlib.reload(run_robust_control)
from run_robust_control import run_robust


X,U,timings,wall_dist = run_robust(chain_params_gpprior)

# %%
timings

# %%
wall_dist

# %%
from utils import *
from plot_utils import *

IDs = ["nominal", "zoro", "robust", "gpzoro"]
Seeds = range(50,51)

for n_mass in range(5,6):

    wall_dist = {id:[] for id in IDs}

    # load results
    for id in IDs:
        for seed in Seeds:
            chain_params["seed"] = seed
            chain_params["n_mass"] = n_mass
            results = load_results_from_json(id, chain_params)
            wall_dist[id] += results["wall_dist"]

    # timings_plot(timings, n_mass)

    params = get_latex_plot_params()
    matplotlib.rcParams.update(params)

    # actual plot
    IDs = wall_dist.keys()
    # plot timings
    fig = plt.figure()
    ax = plt.gca()

    # fig2,ax2 = plt.subplots()
    n_bins = 300
    

    for id in IDs:
        wd = wall_dist[id]
        n, bins = np.histogram(wd,bins=n_bins,density=True)
        n = np.cumsum(n)
        n /= np.max(n)
        ax.plot(bins[0:-1], n, '--', label=id)

    # ax.axhline(1 - chain_params["prob_x"], color='k')
    ax.axvline(0, color='k')

    # ax.set_yscale('log')
    # ax.set_xscale('log')
    ax.set_yticks(np.hstack((
        np.linspace(0,1,5),
        1 - chain_params["prob_x"]
    )))
    ax.set_ylabel("empirical probability")
    ax.set_xlabel("distance to wall")
    ax.grid(True)
    ax.set_title("Wall constraint violation for " + str(n_mass) + " masses")
    ax.legend()
    fig.savefig("figures/performance_nm" + str(n_mass) + ".pdf",\
        bbox_inches='tight', transparent=True, pad_inches=0.05)

plt.show()

# %%
