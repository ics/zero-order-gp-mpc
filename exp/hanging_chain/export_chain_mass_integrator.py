# ©2022-​2023 ETH Zurich, Amon Lahr, Andrea Zanelli, Andrea Carron, Melanie N. Zeilinger; D-MAVT; Institute for Dynamical Systems and Control; Intelligent Control Systems Group

import sys
import numpy as np
import scipy.linalg

from acados_template import AcadosSim, AcadosOcpSolver, AcadosSimSolver

from export_chain_mass_model import export_chain_mass_model
from export_disturbed_chain_mass_model import export_disturbed_chain_mass_model

import matplotlib.pyplot as plt

# create ocp object to formulate the simulation problem
sim = AcadosSim()


def export_chain_mass_integrator(model, Ts):

    # simulation options
    # Ts = 0.2

    # export model
    # M = n_mass - 2 # number of intermediate masses
    # model = export_disturbed_chain_mass_model(n_mass, m, D, L)
    # model = export_chain_mass_model(n_mass, m, D, L, wind_amp=wind_amp)

    # set model
    sim.model = model

    # disturbances
    # nparam = 3*M
    if model.p:
        sim.parameter_values = np.zeros(model.p.shape)

    # solver options
    sim.solver_options.integrator_type = 'IRK'
    # sim.solver_options.integrator_type = 'ERK'

    sim.solver_options.sim_method_num_stages = 2
    sim.solver_options.sim_method_num_steps = 5# 1
    # sim.solver_options.nlp_solver_tol_eq = 1e-9

    # set prediction horizon
    sim.solver_options.Tsim = Ts

    acados_integrator = AcadosSimSolver(sim, json_file = 'acados_sim_' + model.name + '.json')

    return acados_integrator, sim