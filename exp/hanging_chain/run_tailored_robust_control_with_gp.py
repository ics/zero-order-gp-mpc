# ©2022-​2023 ETH Zurich, Amon Lahr, Andrea Zanelli, Andrea Carron, Melanie N. Zeilinger; D-MAVT; Institute for Dynamical Systems and Control; Intelligent Control Systems Group

import sys, os
sys.path += ["../../src/"]

import torch

from acados_template import AcadosSimSolver

from export_chain_mass_integrator import export_chain_mass_integrator
from hanging_chain_model_acados import export_ocp
from hanging_chain_gp_utils import *
from utils import *
from plot_utils import *
from zoro_acados_utils import get_total_timings_per_task, get_total_timings, get_total_iter, timings_names_raw

from zoro_acados import ZoroAcados
import json

def run_gpzoro(chain_params, add_train_data=None):
    # auxiliary
    print(chain_params)
    chain_params_aux = get_chain_params_utilities(chain_params)
    np.random.seed(chain_params["seed"])
    torch.set_num_threads(chain_params["gp_train_params"]["cpu_cores"])
    torch.set_num_interop_threads(1)

    # gp model 
    if chain_params["gp_train_params"]["N_x0"] > 0:
        gp_model, likelihood = get_gp_model_from_chain_params(
            chain_params, 
            chain_params_aux=chain_params_aux
        )

    # export ocp without wind consideration
    ocp, sim = export_ocp(
        chain_params, 
        chain_params_aux=chain_params_aux,
        include_wind=False,
        include_covariances_as_params=True
    )

    # zoro solver
    if chain_params["gp_train_params"]["N_x0"] > 0:
        zoro_solver = ZoroAcados(
            ocp, 
            sim, 
            chain_params["prob_x"], 
            chain_params_aux["Sigma_x0"], 
            chain_params_aux["Sigma_W"], 
            B=chain_params_aux["B"],
            gp_model=gp_model
        )
    else:
        zoro_solver = ZoroAcados(
            ocp, 
            sim, 
            chain_params["prob_x"], 
            chain_params_aux["Sigma_x0"], 
            chain_params_aux["Sigma_W"]+chain_params_aux["Sigma_W_add"], 
            B=chain_params_aux["B"]
        )

    nx = ocp.model.x.size()[0]
    nu = ocp.model.u.size()[0]

    # closed-loop integrator
    chain_params_cl = chain_params.copy()
    _,sim_cl = export_ocp(
        chain_params_cl,
        include_wind=True
    )
    integrator_cl = AcadosSimSolver(sim_cl)

    # simulate
    N_sim = int(np.floor(chain_params["Tsim"]/chain_params["Ts"]))
    X_sim = np.zeros((N_sim+1,nx))
    U_sim = np.zeros((N_sim,nu))
    solve_data = []

    # initial condition automatically?
    X_sim[0,:] = chain_params_aux["x0"]
    for i in range(N_sim):
        # set lbx, ubx
        zoro_solver.ocp_solver.set(0,"lbx",X_sim[i,:])
        zoro_solver.ocp_solver.set(0,"ubx",X_sim[i,:])

        # solve
        zoro_solver.solve(tol_nlp=chain_params["nlp_tol"], n_iter_max=chain_params["nlp_iter"])
        solve_data += [zoro_solver.get_solve_stats()]
        _,U,_ = zoro_solver.get_solution()
        U_sim[i,:] = U[0,:]

        # simulate
        integrator_cl.set("x", X_sim[i,:])
        integrator_cl.set("u", U_sim[i,:])
        integrator_cl.solve()
        w = np.random.multivariate_normal(
            np.zeros((chain_params_aux["nparam"],)), 
            chain_params_aux["Sigma_W"]
        )
        X_sim[i+1,:] = integrator_cl.get("x") \
            + chain_params_aux["B"] @ w

    # plot results
    if chain_params["show_plots"]:
        plot_chain_control_traj(U_sim)
        plot_chain_position_traj(X_sim, yPosWall=chain_params["yPosWall"])
        plot_chain_velocity_traj(X_sim)
        # plot_chain_position(X_sim[-1,:], xPosFirstMass)
        animate_chain_position(X_sim, chain_params["xPosFirstMass"], yPosWall=chain_params["yPosWall"])
        animate_chain_position_3D(X_sim, chain_params["xPosFirstMass"], Ts=chain_params["Ts"])
        plt.show()

    if chain_params["save_results"]:
        # timings
        # timings = get_timings_from_zoro_acados(solve_data)
        timings_per_task = get_total_timings_per_task(solve_data)
        timings_total = get_total_timings(solve_data, timings_names=timings_names_raw)
        n_iter = get_total_iter(solve_data)
        # wall dist
        wall_dist_all = get_wall_dist_from_traj(chain_params, X_sim)
        wall_dist = np.min(wall_dist_all, axis=1)

        if chain_params["gp_train_params"]["use_cuda"]:
            ID = "gpu-zoro"
        else:
            ID = "gp-zoro"
        save_closed_loop_results_as_json(ID, n_iter, timings_total, timings_per_task, wall_dist, chain_params)

    return X_sim, U_sim, solve_data

if __name__ == "__main__":
    # load chain params from cmd argument
    if len(sys.argv) > 1:
        chain_params_file = sys.argv[1]
    
    dir_path = os.path.dirname(os.path.realpath(__file__))
    chain_params_file_abspath = os.path.join(dir_path, chain_params_file)
    
    with open(chain_params_file_abspath, 'r') as f:
        chain_params = json.load(f)

    # execute function
    run_gpzoro(chain_params)

