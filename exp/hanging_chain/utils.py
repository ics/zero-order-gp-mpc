# ©2022-​2023 ETH Zurich, Amon Lahr, Andrea Zanelli, Andrea Carron, Melanie N. Zeilinger; D-MAVT; Institute for Dynamical Systems and Control; Intelligent Control Systems Group

import sys, os
# sys.path += [os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))),"src")]

root_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
results_dir = os.path.join(root_dir,"exp","hanging_chain","results")
# sys.path += [os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))]

import scipy, json
import numpy as np
import casadi as ca
from export_chain_mass_model import export_chain_mass_model
from export_chain_mass_integrator import export_chain_mass_integrator

def get_chain_params():
    params = dict()
    # model params
    params["n_mass"] = 5
    params["Ts"] = 0.2
    params["Tsim"] = 5
    params["N"] = 40
    params["u_init"] = np.array([-1, 1, 1])
    params["with_wall"] = True
    params["yPosWall"] = -0.05 # Dimitris: - 0.1;
    params["m"] = 0.033 # mass of the balls
    params["D"] = 1.0 # spring constant
    params["L"] = 0.033 # rest length of spring
    params["xPosFirstMass"] = np.zeros((3,1))
    params["wind_amp"] = -0.1
    
    # control params
    params["cost_fac_Q"] = 2.0
    params["cost_fac_R"] = 2e-2
    params["perturb_scale"] = 1e-2
    params["perturb_scale_x0"] = 1e-6
    params["prob_x"] = 0.95
    params["Sigma_W_add"] = None

    # GP params
    params["gp_train_params"] = {
        "independent_outputs": True,
        "cpu_cores": 1,
        "use_cuda": True,
        "N_sim_per_x0": 1,
        "N_sim": 15,
        "N_x0": 1,
        "random_seed": 12345,
        "x0_rand_scale": 0.5,
        "train_iter": 300,
        "solver": "nominal",
        "gp_hyperparams_path": None
    }

    # options
    params["save_results"] = True
    params["show_plots"] = False
    params["nlp_iter"] = 50
    params["seed"] = 50
    params["nlp_tol"] = 5e-5

    return params

def compute_init_state(n_mass, m, D, L, xrest, u_init, wind_amp=0.0, n_iter=5, Ts=0.2):
    model = export_chain_mass_model(n_mass, m, D, L, wind_amp=wind_amp)
    acados_integrator,_ = export_chain_mass_integrator(model, Ts) # TODO: get Ts from chain params
    
    # get initial state from xrest
    xcurrent = xrest
    for i in range(5):
        acados_integrator.set("x", xcurrent)
        acados_integrator.set("u", u_init)

        status = acados_integrator.solve()
        if status != 0:
            raise Exception('acados integrator returned status {}. Exiting.'.format(status))

        # update state
        xcurrent = acados_integrator.get("x")

    return xcurrent

def compute_steady_state(n_mass, m, D, L, xPosFirstMass, xEndRef, wind_amp=0.0):
    
    model = export_chain_mass_model(n_mass, m, D, L, wind_amp=0.0)
    nx = model.x.shape[0]
    M = int((nx/3 -1)/2)
    
    # initial guess for state
    pos0_x = np.linspace(xPosFirstMass[0], xEndRef[0], n_mass)
    x0 = np.zeros((nx, 1))
    x0[:3*(M+1):3] = pos0_x[1:].reshape((M+1,1))
    
    # decision variables
    w = [model.x, model.xdot, model.u]
    # initial guess
    w0 = ca.vertcat(*[x0, np.zeros(model.xdot.shape), np.zeros(model.u.shape)])
    
    # constraints
    g = []
    g += [model.f_impl_expr]                        # steady state
    g += [model.x[3*M:3*(M+1)]  - xEndRef]          # fix position of last mass
    g += [model.u]                                  # don't actuate controlled mass
    
    # misuse IPOPT as nonlinear equation solver
    nlp = {'x': ca.vertcat(*w), 'f': 0, 'g': ca.vertcat(*g)}
    
    solver = ca.nlpsol('solver', 'ipopt', nlp)
    sol = solver(x0=w0,lbg=0,ubg=0)
    
    wrest = sol['x'].full()
    xrest = wrest[:nx]

    return xrest

def compute_xEndRef(n_mass, L):
    M = n_mass - 2

    xEndRef = np.zeros((3,1))
    xEndRef[0] = L * (M+1) * 6

    return xEndRef

def get_chain_params_utilities(chain_params):
    # chain parameters
    n_mass = chain_params["n_mass"]
    M = chain_params["n_mass"] - 2 # number of intermediate masses
    Ts = chain_params["Ts"]
    Tsim = chain_params["Tsim"]
    N = chain_params["N"]
    u_init = np.array(chain_params["u_init"])
    with_wall = chain_params["with_wall"]
    yPosWall = chain_params["yPosWall"]
    m = chain_params["m"]
    D = chain_params["D"]
    L = chain_params["L"]
    perturb_scale = chain_params["perturb_scale"]
    perturb_scale_x0 = chain_params["perturb_scale_x0"]
    xPosFirstMass = chain_params["xPosFirstMass"]
    wind_amp = chain_params["wind_amp"]
    prob_x = chain_params["prob_x"]

    nlp_iter = chain_params["nlp_iter"]
    nlp_tol = chain_params["nlp_tol"]
    save_results = chain_params["save_results"]
    show_plots = chain_params["show_plots"]
    seed = chain_params["seed"]

    # compute xEndRef
    xEndRef = compute_xEndRef(n_mass, L)

    # compute xrest
    xrest = compute_steady_state(n_mass, m, D, L, xPosFirstMass, xEndRef)

    # compute x0
    x0 = compute_init_state(n_mass, m, D, L, xrest, u_init, wind_amp=0.0, n_iter=5)

    # compute B, Sigma_W, Sigma_x0 from perturb_scale
    nx = 6 * M + 3
    nparam = 3*M
    Sigma_x0 = perturb_scale_x0 * np.eye(nx)
    Sigma_W = perturb_scale * Ts * np.eye(nparam)
    if chain_params["Sigma_W_add"] is None:
        Sigma_W_add = np.zeros((nparam,nparam))
    else:
        Sigma_W_add = np.array(chain_params["Sigma_W_add"])


    B = np.vstack(( np.zeros((nx - nparam, nparam)), np.eye(nparam)))

    aux = {
        "nx": nx,
        "nu": 3,
        "M": M,
        "nparam": nparam,
        "xrest": xrest,
        "x0": x0,
        "B": B,
        "Sigma_x0": Sigma_x0,
        "Sigma_W": Sigma_W,
        "Sigma_W_add": Sigma_W_add
    }
    return aux



def sampleFromEllipsoid(w, Z):
    """
    draws uniform(?) sample from ellipsoid with center w and variability matrix Z
    """

    n = w.shape[0]                  # dimension
    lam, v = np.linalg.eig(Z)

    # sample in hypersphere
    r = np.random.rand()**(1/n)     # radial position of sample
    x = np.random.randn(n)
    x = x / np.linalg.norm(x)
    x *= r
    # project to ellipsoid
    y = v @ (np.sqrt(lam) * x) + w

    return y


def sym_mat2vec(mat):
    nx = mat.shape[0]

    if isinstance(mat, np.ndarray):
        vec = np.zeros((int((nx+1)*nx/2),))
    else:
        vec = ca.SX.zeros(int((nx+1)*nx/2))

    start_mat = 0
    for i in range(nx):
        end_mat = start_mat + (nx - i)
        vec[start_mat:end_mat] = mat[i:,i]
        start_mat += (nx-i)

    return vec


def vec2sym_mat(vec, nx):
    # nx = (vec.shape[0])

    if isinstance(vec, np.ndarray):
        mat = np.zeros((nx,nx))
    else:
        mat = ca.SX.zeros(nx,nx)

    start_mat = 0
    for i in range(nx):
        end_mat = start_mat + (nx - i)
        aux = vec[start_mat:end_mat]
        mat[i,i:] = aux.T
        mat[i:,i] = aux
        start_mat += (nx-i)

    return mat


def is_pos_def(mat):
    try:
        np.linalg.cholesky(mat)
        return 1
    except np.linalg.linalg.LinAlgError as err:
        if 'Matrix is not positive definite' in err.args[0]:
            return 0
        else:
            raise


def P_propagation(P, A, B, W):
    #  P_i+1 = A P A^T +  B*W*B^T
    return A @ P @ A.T + B @ W @ B.T


def np_array_to_list(np_array):
    if isinstance(np_array, (np.ndarray)):
        return np_array.tolist()
    elif isinstance(np_array, (SX)):
        return DM(np_array).full()
    elif isinstance(np_array, (DM)):
        return np_array.full()
    else:
        raise(Exception(
            "Cannot convert to list type {}".format(type(np_array))
        ))


def save_results_as_json(result_dict, json_file):

    dir = os.path.dirname(os.path.realpath(__file__))
    json_file_path = os.path.join(dir, json_file)
    with open(json_file_path, 'w') as f:
        json.dump(result_dict, f, default=np_array_to_list, indent=4, sort_keys=True)

    return

def save_closed_loop_results_as_json(ID, n_iter, timings, timings_per_task, wall_dist, chain_params):

    result_dict = dict()

    result_dict["timings"] = timings
    result_dict["timings_per_task"] = timings_per_task
    result_dict["n_iter"] = n_iter
    result_dict["wall_dist"] = wall_dist
    result_dict["chain_params"] = chain_params

    json_file = os.path.join("results",
        f"{ID}_cores{chain_params['gp_train_params']['cpu_cores']}_nm_{chain_params['n_mass']}_nGP_{chain_params['gp_train_params']['N_x0']*chain_params['gp_train_params']['N_sim']}_iter_{chain_params['nlp_iter']}_seed_{chain_params['seed']}.json"
    )

    save_results_as_json(result_dict, json_file)

    return

def load_results_from_json(ID, chain_params):
    json_file = os.path.join(results_dir,
        f"{ID}_cores{chain_params['gp_train_params']['cpu_cores']}_nm_{chain_params['n_mass']}_nGP_{chain_params['gp_train_params']['N_x0']*chain_params['gp_train_params']['N_sim']}_iter_{chain_params['nlp_iter']}_seed_{chain_params['seed']}.json"
    )
    
    with open(json_file, 'r') as f:
        results = json.load(f)

    return results
